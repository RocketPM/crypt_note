
import UIKit

class ItemDetailViewController: UIViewController {

    
    @IBOutlet weak var itemTextfield: UITextField!
    
    var allItems = [String]()
    var currentItemNumber = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        itemTextfield.text = allItems[currentItemNumber]
    }
    

    @IBAction func saveItem(_ sender: Any) {
        
        allItems[currentItemNumber] = itemTextfield.text!
        
        UserDefaults.standard.set(allItems, forKey: "items")
        
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func deleteItem(_ sender: Any) {
        
        allItems.remove(at: currentItemNumber)
        
        UserDefaults.standard.set(allItems, forKey: "items")
        
        navigationController?.popViewController(animated: true)
        
    }
    
    
    
}
