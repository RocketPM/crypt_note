//APP MED CUSTOM Tableview cell

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var addItemTextfield: UITextField!
    @IBOutlet weak var itemsTableview: UITableView!
    
    var theItems = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*
        theItems.append("Apa")
        theItems.append("Banan")
        theItems.append("Äpple")
        theItems.append("Kiwi")
        */
        
        /*
         // FUNKAR MEN INTE SNYGGT
        if(UserDefaults.standard.object(forKey: "items") != nil)
        {
            // HÄMTA SPARAT VÄRDE
        }
        */
        
        
        
        //theItems = UserDefaults.standard.object(forKey: "items") as! [String]
        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        
        if let theSavedList = UserDefaults.standard.object(forKey: "items") as? [String]
        {
            theItems = theSavedList
        }
        
        itemsTableview.reloadData()
        
    }
    
    
    
    @IBAction func addItem(_ sender: Any) {
        letsAddItem()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return theItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listcell") as! ItemTableViewCell
        
        cell.itemNameLabel.text = theItems[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("TRYCK PÅ RAD \(indexPath.row)")
        
        performSegue(withIdentifier: "itemdetail", sender: indexPath.row)
        
        /*
        theItems.remove(at: indexPath.row)
        itemsTableview.reloadData()
        
        UserDefaults.standard.set(theItems, forKey: "items")
        */
        /*
         0: Apa
         1: banan
         2: Persika
 
         remove(1)
         
         0: Apa
         1: persika
         
        */
        
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("HEY RETURN CLICKING")
        letsAddItem()
        
        return true
    }
    
    func letsAddItem()
    {
        if(addItemTextfield.text == "")
        {
            // Sluta köra funktionen
            return
        }
        
        theItems.append(addItemTextfield.text!)
        itemsTableview.reloadData()
        
        addItemTextfield.text = ""
        
        view.endEditing(true)
        
        UserDefaults.standard.set(theItems, forKey: "items")

    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dest = segue.destination as! ItemDetailViewController
        
        dest.allItems = theItems
        dest.currentItemNumber = sender as! Int
        
    }
    
    
    
    
}

